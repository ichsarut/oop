<?php

// Adapter Pattern ทำงานเหมือนเป็นสะพานที่เชื่อมของสองอย่างที่ไม่เหมือนกัน เข้าด้วยกัน
// รูปแบบนี้เป็น Single Class ซึ่งมีความรับผิดชอบที่จะทำให้ของสองอย่างที่เป็นอิสระจากกันและแตกต่างจากกันให้ทำงานร่วมกันได้
// ถ้าเปรียบเทียบในชีวิตจริง ๆ ก็มี Card Reader ที่ทำหน้าที่เป็น Adapter ระหว่าง Memory Card และ Laptop
// เราใส่ Memory Card ใน Card Reader และเสียบ Card Reader เข้ากับ Laptop เราจึงจะอ่านข้อมูลจาก memory card ได้

namespace DesignPattern\Structural\Adapter;

class SimpleBook
{
    private $author;
    private $title;

    public function __construct($author_in, $title_in)
    {
        $this->author = $author_in;
        $this->title = $title_in;
    }

    function getAuthor()
    {
        return $this->author;
    }

    function getTitle()
    {
        return $this->title;
    }
}

class BookAdapter
{
    private $book;

    function __construct(SimpleBook $book_in)
    {
        $this->book = $book_in;
    }

    function getAuthorAndTitle()
    {
        return $this->book->getTitle() . ' by ' . $this->book->getAuthor();
    }
}

writeln('BEGIN TESTING ADAPTER PATTERN');
writeln('');

$book = new SimpleBook("Gamma, Helm, Johnson, and Vlissides", "Design Patterns");
$bookAdapter = new BookAdapter($book);
writeln('Author and Title: ' . $bookAdapter->getAuthorAndTitle());
writeln('');

writeln('END TESTING ADAPTER PATTERN');

function writeln($line_in)
{
    echo $line_in . "<br/>";
}

