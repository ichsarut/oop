<?php
namespace DesignPattern\Creational\SimpleFactory;

/*
 * จุดประสงค์
 * มันแตกต่างจาก static factory เพราะว่ามันไม่ใช่ static(ค่าคงที่)
 * เพราะฉะนั้นคุณสามารถมีได้หลาย factories ด้วย parameter ที่แตกต่างกัน
 *
 */

class Bicycle
{
    public function driveTo(string $destination)
    {
        echo "Drive to => " . $destination;
    }
}

class SimpleFactory
{
    public function createBicycle()
    {
        return new Bicycle();
    }
}

$factory = new SimpleFactory();
$bicycle = $factory->createBicycle();
$bicycle->driveTo('Bangkok');