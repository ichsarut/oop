<?php

namespace DesignPattern\Structural\Decorator;

interface Car
{
    function cost();
    function description();
}


class Suv implements Car
{
    function cost()
    {
        return 30000;
    }

    function description()
    {
        return "Suv";
    }
}

abstract class CarDecorate implements Car {
    protected $car;

    function __construct(Car $car)
    {
        $this->car = $car;
    }

    abstract function cost();
    abstract function description();
}

class SunRoof extends CarDecorate {
    function cost ()
    {
        return $this->car->cost() + 1500;
    }

    function description()
    {
        return $this->car->description() . ",  sunroof";
    }
}

class HighEndWheels extends CarDecorate {
    function cost ()
    {

        return $this->car->cost() + 2000;
    }

    function description()
    {
        return $this->car->description() . ",  high end wheels";
    }
}

/**
 * การสร้าง
 */
$basicCar = new Suv();
$carWithSunRoof = new SunRoof($basicCar);
echo $carWithSunRoof -> description();
echo " costs ";
echo $carWithSunRoof -> cost();
echo '<br>';

$basicCar = new Suv();
$carWithSunRoof = new SunRoof($basicCar);
$carWithSunRoofAndHighEndWheels = new HighEndWheels($carWithSunRoof);
echo $carWithSunRoofAndHighEndWheels -> description(); // เรียก function description ย้อนขึ้นไปตั้งแต่ลำดับแรก ๆ
echo " costs ";
echo $carWithSunRoofAndHighEndWheels -> cost();





