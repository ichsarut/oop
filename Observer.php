<?php

namespace DesignPattern\Behavioral\Observer;

// ในกรณีที่เป็นความสัมพันธ์แบบ 1:M เช่นถ้ามี object หนึ่งถูกเปลี่ยนแปลง อะไรก็ตามที่ขึ้นกับ object ตัวนั้น
// จะถูกแจ้งเตือนให้ทราบถึงการเปลี่ยนแปลงนั้นโดยอัตโนมัติ

// ในตัวอย่างนี้ ใช้ 3 classes ในการทำงาน Subject Observer และ Client
// Subject มีความสามารถในการเพิ่มและลบ Observer == attach() และ detach() ไปยัง Client Object ได้


class Subject
{
    private $observers = array(); // เก็บ Observer ทั้งหมดไว้ในตัวหลัก
    private $state;

    public function getState()
    {
        return $this->state;
    }

    public function setState($state) // เมื่อมีการ update ข้อมูล state ของตัวหลัก ก็จะแจ้ง object ที่ติดตามอยู่ทั้งหมด
    {
        $this->state = $state;
        $this->notifyAllObservers();
    }

    function attach(Observer $observer)
    {
        $this->observers[] = $observer;
    }

    function notifyAllObservers()
    {
        foreach ($this->observers as $observer) {
            $observer->update();
        }
    }

}

abstract class Observer
{
    protected $subject;
    abstract function update();     // สร้าง function รองรับการแจ้งเตือนจาก Subject
}

class BinaryObserver extends Observer
{
    protected $subject;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
        $this->subject->attach($this);
    }

    function update()
    {
        writeln("Binary String: " . decbin($this->subject->getState()));
    }
}

class OctalObserver extends Observer
{
    protected $subject;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
        $this->subject->attach($this);
    }

    function update()
    {
        writeln("Octal String: " . decoct($this->subject->getState()));
    }
}

class HexaObserver extends Observer
{
    protected $subject;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
        $this->subject->attach($this);
    }

    function update()
    {
        writeln("Hexa String: " . dechex($this->subject->getState()));
    }
}

$subject = new Subject();

new HexaObserver($subject);
new OctalObserver($subject);
new BinaryObserver($subject);

writeln("First state change: 15");
$subject->setState(15);
writeln("Second state change: 10");
$subject->setState(10);
writeln("Second state change: 5");
$subject->setState(5);


function writeln($lineIn)
{
    echo $lineIn . "<br/>";
}