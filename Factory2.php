<?php
namespace DesignPattern\Creational\Factory2;

interface Payment {
    public function createTransaction();
    public function callbackTransaction();
}

class Paypal implements Payment {
    public function createTransaction() {
        echo __CLASS__ . " Create Transaction 2 <br>";
    }

    public function callbackTransaction()
    {
        echo __CLASS__ . " Callback Transaction <br>";
    }
}

class CreditCard implements Payment {
    public function createTransaction() {
        echo __CLASS__ . " Create Transaction 2 <br>";
    }

    public function callbackTransaction()
    {
        echo __CLASS__ . " Callback Transaction <br>";
    }
}

class PaymentFactory {

    // Factory method;
    public static function paymentGateway($paymentType) {
        $targetClass = ucfirst($paymentType);
        switch ($targetClass) {
            case 'Paypal':
                return new Paypal();
                break;
            case 'CreditCard':
                return new CreditCard();
                break;
            default:
                echo "default";
                return null;
        }
    }
}

$payment = new PaymentFactory();
$paypal = $payment->paymentGateway('paypal');
$credit = $payment->paymentGateway('CreditCard');

$paypal->createTransaction();
$paypal->callbackTransaction();

echo "<hr>";

$credit->createTransaction();
$credit->callbackTransaction();