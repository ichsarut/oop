<?php

namespace OOP\Interface2;


interface StandardPaymentInterface {
    public function pay();
    public function process();
}

interface FraudCheckInterface {
    public function fraudCheck();
}

interface ThreeDCheckInterface {
    public function threeDCheck();
}


class PayFee implements StandardPaymentInterface, ThreeDCheckInterface {
    public function pay() {}
    public function threeDCheck() {}
    public function process() {
        $this->threeDCheck();
        $this->pay();
    }
}

class WorldFee implements StandardPaymentInterface {
    public function pay() {}
    public function process() {
        $this->pay();
    }
}

class MintFee implements StandardPaymentInterface, FraudCheckInterface {
    public function pay() {}
    public function fraudCheck() {}
    public function process() {
        $this->process();
    }
}


class PaymentGateway {
    public function takePayment(StandardPaymentInterface $paymentType) {
        $paymentType->pay();
    }
}

$payment = new PayFee();
$gateway = new PaymentGateway();
$gateway->takePayment($paymentType);