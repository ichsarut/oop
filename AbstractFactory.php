<?php

namespace DesignPattern\Creational\AbstractFactory;

interface ShapeInterface
{
    public function draw();
}

interface Color
{
    public function fill();
}

abstract class AbstractFactory
{
    abstract function getColor($color);

    abstract function getShape($shapeType);
}

class Rectangle implements ShapeInterface
{
    public function draw()
    {
        echo "Inside Rectangle::draw() method.";
    }
}

class Square implements ShapeInterface
{
    public function draw()
    {
        echo "Inside Square::draw() method.";
    }
}

class Circle implements ShapeInterface
{
    public function draw()
    {
        echo "Inside Circle::draw() method.";
    }
}


class Red implements Color
{
    public function fill()
    {
        echo "Inside Red::fill() method.";
    }
}

class Green implements Color
{
    public function fill()
    {
        echo "Inside Green::fill() method.";
    }
}

class Blue implements Color
{
    public function fill()
    {
        echo "Inside Blue::fill() method.";
    }
}


class ShapeFactory extends AbstractFactory
{
    public function getColor($color)
    {
        return null;
    }

    public function getShape($shapeType)
    {
        if ($shapeType == null) {
            return null;
        }

        switch ($shapeType) {
            case  'RECTANGLE' :
                return new Rectangle();
                break;
            case  'SQUARE' :
                return new Square();
                break;
            case  'CIRCLE' :
                return new Circle();
                break;
            default :
                return null;
        }
    }
}

class ColorFactory extends AbstractFactory
{
    public function getColor($color)
    {
        if ($color == null) {
            return null;
        }

        switch ($color) {
            case  'RED' :
                return new Red();
                break;
            case  'GREEN' :
                return new Green();
                break;
            case  'BLUE' :
                return new Blue();
                break;
            default :
                return null;
        }
    }

    public function getShape($shapeType)
    {
        return null;
    }
}

