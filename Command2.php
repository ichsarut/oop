<?php

namespace DesignPattern\Behavioral\Command2;

function writeln($line_in) {
    echo $line_in."<br/>";
}

interface ElectronicDevices {
    function on();
    function off();
    function volumeUp();
    function volumeDown();
}

class Television implements ElectronicDevices {
    private $volume = 0;

    function on()
    {
        writeln('TV is on');
    }

    function off()
    {
        writeln('TV is off');
    }

    function volumeUp()
    {
        $this->volume++;
        writeln('TV Volume is at: ' . $this->volume);
    }

    function volumeDown()
    {
        $this->volume--;
        writeln('TV Volume is at: ' . $this->volume);
    }
}

interface Command {
    function execute();
    function undo();
}

class TurnDeviceOn implements Command {
    private $theDevice;
    public function __construct(ElectronicDevices $device)
    {
        $this->theDevice = $device;
    }

    function execute()
    {
        $this->theDevice->on();
    }

    function undo()
    {
        $this->theDevice->off();
    }
}

class TurnDeviceOff implements Command {
    private $theDevice;
    public function __construct(ElectronicDevices $device)
    {
        $this->theDevice = $device;
    }

    function execute()
    {
        $this->theDevice->off();
    }

    function undo()
    {
        $this->theDevice->on();
    }
}

class TurnDeviceUp implements Command {
    private $theDevice;
    public function __construct(ElectronicDevices $device)
    {
        $this->theDevice = $device;
    }

    function execute()
    {
        $this->theDevice->volumeUp();
    }

    function undo()
    {
        $this->theDevice->volumeDown();
    }
}

class TurnDeviceDown implements Command {
    private $theDevice;
    public function __construct(ElectronicDevices $device)
    {
        $this->theDevice = $device;
    }

    function execute()
    {
        $this->theDevice->volumeDown();
    }

    function undo()
    {
        $this->theDevice->volumeUp();
    }
}


class DeviceButton {
    private $theCommand;
    public function __construct(Command $newCommand) {
        $this->theCommand = $newCommand;
    }

    public function press() {
        $this->theCommand->execute();
    }

    public function undo() {
        $this->theCommand->undo();
    }
}

class Remote {
    public static function getDevice() {
        return new Television();
    }
}

$newDevice = Remote::getDevice();

$onCommand = new TurnDeviceOn($newDevice);
$onPress = new DeviceButton($onCommand);
$onPress->press();
