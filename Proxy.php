<?php
namespace DesignPattern\Structural\Proxy;

/**
 * Proxy Pattern
 *
 * จุดประสงค์
 * - จัดเตรียม object ที่ทำงานแทน object อื่น เพื่่อควบคุมการเข้าถึง object นั้น ๆ
 * - ทำหน้าที่เป็นตัวแทนในการจัดการข้อมูลแทน object อื่น ๆ
 * -
 *
 *
 */

interface Image
{
    public function display();
}

class RealImage implements Image
{
    private $fileName;

    function __construct($fileName)
    {
        $this->fileName = $fileName;
        $this->loadFromDisk($fileName);
    }

    public function display()
    {
        echo "Displaying " . $this->fileName . "<br>";
    }

    private function loadFromDisk()
    {
        echo "Loading " . $this->fileName . "<br>";
    }

}

// class ทำหน้าที่เป็นตัวแทนจัดการข้อมูล ก่อนจะที่ทำอะไรต่อกับ class หลัก
class ProxyImage implements Image
{
    private $realImage;
    private $fileName;

    function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    public function display()
    {
        if ($this->realImage == null) {
            $this->realImage = new RealImage($this->fileName);
        }
        $this->realImage->display();
    }
}

//
$image = new ProxyImage("test_10mb.jpg");
// ภาพจะถูกโหลดจาก Disk ในครั้งแรก RealImage::loadFromDisk()
$image->display();
echo "<br>";

// ภาพจะไม่ถูกโหลดจาก Disk
$image->display();