<?php
namespace DesignPattern\Structural\Composite;

abstract class OnTheBookShelf {
    abstract function getBookInfo($previousBook);
    abstract function getBookCount();
    abstract function setBookCount($newCount);
    abstract function addBook($oneBook);
    abstract function removeBook($oneBook);
}

class OneBook extends OnTheBookShelf
{
    private $title;
    private $author;

    function __construct($title, $author) {
        $this->title = $title;
        $this->author = $author;
    }

    function getBookInfo($bookToGet)
    {
        if (1 == $bookToGet) {
            return $this->title." by ".$this->author;
        } else {
            return FALSE;
        }
    }

    function getBookCount()
    {
        return 1;
    }

    function setBookCount($newCount)
    {
        return FALSE;
    }

    function addBook($oneBook)
    {
        return FALSE;
    }

    function removeBook($oneBook)
    {
        return FALSE;
    }
}


class SeveralBooks extends OnTheBookShelf
{
    private $oneBooks = array();
    private $bookCount;

    public function __construct()
    {
        $this->setBookCount(0);
    }

    function getBookInfo($bookToGet) {
        if ($bookToGet <= $this->bookCount) {
            return $this->oneBooks[$bookToGet]->getBookInfo(1);
        } else {
            return FALSE;
        }
    }

    function getBookCount()
    {
        return $this->bookCount;
    }

    function setBookCount($newCount)
    {
        $this->bookCount = $newCount;
    }

    function addBook($oneBook) {
        $this->setBookCount($this->getBookCount() + 1);
        $this->oneBooks[$this->getBookCount()] = $oneBook;
        return $this->getBookCount();
    }

    function removeBook($oneBook) {
        $counter = 0;
        while (++$counter <= $this->getBookCount()) {
            echo $counter;
            /** check info is equal to this one in list? */
            if ($oneBook->getBookInfo(1) == $this->oneBooks[$counter]->getBookInfo(1)) {
                for ($x = $counter; $x < $this->getBookCount(); $x++) {
                    $this->oneBooks[$x] = $this->oneBooks[$x + 1];
                }
                $this->setBookCount($this->getBookCount() - 1);
            }
        }
        return $this->getBookCount();
    }
}

writeln("BEGIN TESTING COMPOSITE PATTERN");
writeln('');

$firstBook = new OneBook('Core PHP Programming, Third Edition', 'Atkinson and Suraski');
writeln('(after creating first book) oneBook info: ');
writeln($firstBook->getBookInfo(1));
writeln('');

$secondBook = new OneBook('PHP Bible', 'Converse and Park');
writeln('(after creating second book) oneBook info: ');
writeln($secondBook->getBookInfo(1));
writeln('');

$thirdBook = new OneBook('Design Patterns', 'Gamma, Helm, Johnson, and Vlissides');
writeln('(after creating third book) oneBook info: ');
writeln($thirdBook->getBookInfo(1));
writeln('');

writeln('END TESTING COMPOSITE PATTERN');

function writeln($line_in) {
    echo $line_in."<br/>";
}
