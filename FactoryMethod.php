<?php

/**
 *  Factory Method [ Creational Design Pattern ]
 *
 *  วัตถุประสงค์
 *  Factory Method จัดเตรียมส่วนเชื่อมต่อ ( interface ) สำหรับการสร้าง object ใน superclass.
 *  แต่ก็อนุญาติให้ subclasses แก้ไขประเภทของ object ที่จะถูกสร้าง
 *
 *  ปัญหา
 *  ลองจิตนาการถ้าคุณมี Login สำหรับการจัดการ application
 *  version แรกของ app  สามารถดูแลได้เฉพาะการขนส่งด้วยรถบรรทุก
 *  ซึ่ง Code ทั้งหมดอยุ่ใน Truck Class
 *
 *  หลังจากนั้น app ได้รับความนิยมมาก และผู้ใช้เรียกร้องให้มีการขนส่งทางทะเล
 *
 *  มันดูดีใช่ไหมละ แล้ว Code ละ แต่ดูเหมือนว่า Code ทั้งหมดเนี้ยผูกติดไปกับรถบรรทุกไปซะละ(Truck)
 *  การเพิ่ม Ships จะต้องเปลี่ยน code ทั้งหมด
 *  แล้วถ้ายิ่งเพิ่มเติมการขนส่งอื่น ๆเข้าไปอีกละจะต้องแก้ไขโค้ดอีกรอบเหรอ ?
 *
 *  มันจะจบด้วย code ที่พันกันวุ่นวายไปหมดเกี่ยวกับเงื่อนไขต่าง ๆ ที่เลือกใช้ในการขนส่งแต่ละประเภท
 *
 *  ทางแก้ไข
 *  Factory Method แนะนำให้
 *
 */

namespace DesignPattern\Creational\FactoryMethod;

interface Logger
{
    public function log(string $message);
}


class StdoutLogger implements Logger
{
    public function log(string $message)
    {
        echo $message;
    }
}

class FileLogger implements Logger
{
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function log(string $message)
    {
        file_put_contents($this->filePath, $message . PHP_EOL, FILE_APPEND);
    }
}

// =========================================

interface LoggerFactory
{
    public function createLogger(): Logger;
}

class StdoutLoggerFactory implements LoggerFactory
{
    public function createLogger(): Logger
    {
        return new StdoutLogger();
    }
}

class FileLoggerFactory implements LoggerFactory
{
    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function createLogger(): Logger
    {
        return new FileLogger($this->filePath);
    }
}

use PHPUnit\Framework\TestCase;

class FactoryMethodTest extends TestCase
{
    public function testCanCreateStdoutLogging()
    {
        $loggerFactory = new StdoutLoggerFactory();
        $logger = $loggerFactory->createLogger();

        $this->assertInstanceOf(StdoutLogger::class, $logger);
    }

    public function testCanCreateFileLogging()
    {
        $loggerFactory = new FileLoggerFactory(sys_get_temp_dir());
        $logger = $loggerFactory->createLogger();

        $this->assertInstanceOf(FileLogger::class, $logger);
    }
}