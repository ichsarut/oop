<?php

namespace DesignPattern\Structural\Proxy2;

class Book
{
    private $author;
    private $title;

    function __construct($title, $author)
    {
        $this->title = $title;
        $this->author = $author;
    }

    function getAuthor()
    {
        return $this->author;
    }

    function getTitle()
    {
        return $this->title;
    }

    function getAuthorAndTitle()
    {
        return $this->getTitle() . " by " . $this->getAuthor();
    }
}

class BookList
{
    private $books = array();
    private $bookCount = 0;

    function __construct()
    {
    }

    public function getBookCount() {
        return $this->bookCount;
    }

    private function setBookCount($newCount)
    {
        $this->bookCount = $newCount;
    }

    public function getBook($bookNumber)
    {
        if ((is_numeric($bookNumber)) && ($bookNumber <= $this->getBookCount())) {
            return $this->books[$bookNumber];
        } else {
            return NULL;
        }
    }

    public function addBook(Book $newBook)
    {
        $this->setBookCount($this->getBookCount() + 1);
        $this->books[$this->getBookCount()] = $newBook;
        return $this->getBookCount();
    }

    public function removeBook(Book $bookIn)
    {
        $counter = 0;
        while (++$counter <= $this->getBookCount()) {
            if ($bookIn->getAuthorAndTitle() == $this->books[$counter]->getAuthorAndTitle()) {
                for ($x = $counter; $x < $this->getBookCount(); $x++) {
                    $this->books[$x] = $this->books[$x + 1];
                }
                $this->setBookCount($this->getBookCount() - 1);
            }
        }
        return $this->getBookCount();
    }
}



class ProxyBookList
{
    private $bookList = null;

    function __construct()
    {
    }

    function getBookCount()
    {
        if ($this->bookList == null) {
            $this->makeBookList();
        }

        return $this->bookList->getBookCount();
    }

    function addBook($book)
    {
        if (NULL == $this->bookList) {
            $this->makeBookList();
        }
        return $this->bookList->addBook($book);
    }

    function getBook($bookNumber)
    {
        if (NULL == $this->bookList) {
            $this->makeBookList();
        }
        return $this->bookList->getBook($bookNumber);
    }

    function removeBook($book)
    {
        if (NULL == $this->bookList) {
            $this->makeBookList();
        }
        return $this->bookList->removeBook($book);
    }

    //Create
    function makeBookList()
    {
        $this->bookList = new bookList();
    }
}

function writeln($line_in)
{
    echo $line_in . "<br/>";
}

writeln('BEGIN TESTING PROXY PATTERN');
writeln('');

$proxyBookList = new ProxyBookList();
$inBook = new Book('PHP for Cats', 'Larry Truett');
$proxyBookList->addBook($inBook);

writeln('test 1 - show the book count after a book is added');
writeln($proxyBookList->getBookCount());
writeln('');

writeln('test 2 - show the book');
$outBook = $proxyBookList->getBook(1);
writeln($outBook->getAuthorAndTitle());
writeln('');

$proxyBookList->removeBook($outBook);

writeln('test 3 - show the book count after a book is removed');
writeln($proxyBookList->getBookCount());
writeln('');

writeln('END TESTING PROXY PATTERN');
