<?php

namespace DesignPattern\Behavioral\Visitor2;

interface ComputerPart
{
    function accept(ComputerPartVisitor $computerPartVisitor);
}

class Keyboard implements ComputerPart
{
    function accept(ComputerPartVisitor $computerPartVisitor)
    {
        $computerPartVisitor->visitKeyboard($this);
    }
}

class Monitor implements ComputerPart
{
    function accept(ComputerPartVisitor $computerPartVisitor)
    {
        $computerPartVisitor->visitMonitor($this);
    }
}

class Mouse implements ComputerPart
{
    function accept(ComputerPartVisitor $computerPartVisitor)
    {
        $computerPartVisitor->visitMouse($this);
    }
}

class Computer implements ComputerPart
{
    private $parts = array();

    public function __construct()
    {
        $this->parts = [new Mouse(), new Keyboard(), new Monitor()];
    }

    function accept(ComputerPartVisitor $computerPartVisitor)
    {
        for ($i = 0; $i < count($this->parts); $i++) {
            $this->parts[$i]->accept($computerPartVisitor);
        }
        $computerPartVisitor->visitComputer($this);
    }

}


interface ComputerPartVisitor
{
    function visitComputer(Computer $computer);
    function visitMouse(Mouse $mouse);
    function visitKeyboard(Keyboard $keyboard);
    function visitMonitor(Monitor $monitor);
}

class ComputerPartDisplayVisitor implements ComputerPartVisitor
{

    function visitComputer(Computer $computer)
    {
        writeln('Displaying Computer.');
    }

    function visitMouse(Mouse $mouse)
    {
        writeln('Displaying Mouse.');
    }

    function visitKeyboard(Keyboard $keyboard)
    {
        writeln('Displaying Keyboard.');
    }

    function visitMonitor(Monitor $monitor)
    {
        writeln('Displaying Monitor.');
    }
}


$computer = new Computer();
$computer->accept(new ComputerPartDisplayVisitor());


function writeln($lineIn)
{
    echo $lineIn . "<br/>";
}

