<?php
namespace OOP\Interface1;


interface ShapeInterface {
    public function draw();
    public function colour();
    public function reDraw();
}

class Circle implements ShapeInterface {
    public function draw() { return __CLASS__; }
    public function colour() {}
    public function reDraw() {

    }
}

class Square implements ShapeInterface  {
    public function draw() { return __CLASS__; }
    public function colour() {}
    public function reDraw() {

    }
}

class Line implements ShapeInterface  {
    public function draw() { return __CLASS__; }
    public function colour() {}
    public function reDraw() {

    }
}

class Painter {

    // ลดการเขียนโค้ดซ้ำซ้อน
    public function addShape(ShapeInterface $shape) {
        return $shape->draw();
    }
}

$artist = new Painter();

$shape = new Circle();
echo $artist->addShape($shape);

$shape = new Square();
echo $artist->addShape($shape);

$shape = new Line();
echo $artist->addShape($shape);
