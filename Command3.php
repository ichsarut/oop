<?php

namespace DesignPattern\Behavioral\Command3;

function writeln($line_in)
{
    echo $line_in . "<br/>";
}

interface Order
{
    function execute();
}

class Stock
{
    private $name = "ABC";
    private $quantity = 10;

    function buy()
    {
        writeln("Stock [ Name: " . $this->name . ",  Quantity: " . $this->quantity . " ] bought");
    }

    function sell()
    {
        writeln("Stock [ Name: " . $this->name . ",  Quantity: " . $this->quantity . " ] sold");
    }
}

class BuyStock implements Order
{
    private $abcStock;

    public function __construct(Stock $abcStock)
    {
        $this->abcStock = $abcStock;
    }

    function execute()
    {
        $this->abcStock->buy();
    }
}

class SellStock implements Order
{
    private $abcStock;

    public function __construct(Stock $abcStock)
    {
        $this->abcStock = $abcStock;
    }

    function execute()
    {
        $this->abcStock->sell();
    }
}

class Broker
{
    private $orderList = array();

    function takeOrder(Order $order)
    {
        $this->orderList[] = $order;
    }

    function placeOrders()
    {
        foreach ($this->orderList as $order) {
            $order->execute();
        }
        $this->orderList = array();
    }
}

// Request class
$abcStock = new Stock();

// Wrapp request with Command
$buyStockOrder = new BuyStock($abcStock);
$sellStockOrder = new SellStock($abcStock);

// Invoker รับ Request ไป แล้วส่งไปเรียกใช้งานคำสั่ง
$broker = new Broker();
$broker->takeOrder($buyStockOrder);
$broker->takeOrder($sellStockOrder);

$broker->placeOrders();