<?php
namespace DesignPattern\Creational\Factory;


interface ShapeInterface
{
    public function draw();
}

class Rectangle implements ShapeInterface
{

    public function draw()
    {
        echo "Draw " . __CLASS__. " <br>";
    }
}

class Square implements ShapeInterface
{
    public function draw()
    {
        echo "Draw " . __CLASS__. " <br>";
    }
}

class Circle implements ShapeInterface
{
    public function draw()
    {
        echo "Draw " . __CLASS__. " <br>";
    }
}

/*
 * Factory pattern
 * วัตถุประสงค์​
 * สร้าง object สิ่งที่เราอยากได้มันมาโดยที่ไม่จำเป็นต้องรู้กระบวนการในการสร้าง object นั้น ๆ จริง ๆ
 *
 * ข้อดี
 * - information hiding ( Encapsulate ) ซ่อนรายละเอียดการทำงาน
 */

class ShapeFactory
{
    // *** Factory method ***
    public function getShape($shapeType)
    {
        if ($shapeType == null) {
            return null;
        }

        switch ($shapeType) {
            case  'RECTANGLE' :
                return new Rectangle();
                break;
            case  'SQUARE' :
                return new Square();
                break;
            case  'CIRCLE' :
                return new Circle();
                break;
            default :
                return null;
        }
    }
}

$shapeFactory = new ShapeFactory();
$shape1 = $shapeFactory->getShape('RECTANGLE');
$shape1->draw();

$shape2 = $shapeFactory->getShape('SQUARE');
$shape2->draw();

$shape3 = $shapeFactory->getShape('CIRCLE');
$shape3->draw();