<?php

namespace DesignPattern\Behavioral\Visitor;

// ตัวอย่างนี้ แสดงให้เห็นว่า BookVisitee สามารถที่จะเรียกใช้งาน function visitBook() หรือ function อื่น ๆ ที่สืบทอดจาก Visitor class
// โดยการการสร้าง Visitors ขึ้นมาใหม่ ซึ่งรูปแบบข้อมูลของ BookVisitee สามารถเพิ่มได้โดยไม่ต้องเปลี่ยน BookVisitee เลย

abstract class Visitee {
    abstract function accept(Visitor $visitorIn);
}

abstract class Visitor {
    abstract function visitBook(BookVisitee $bookVisiteeIn);
    abstract function visitSoftware(SoftwareVisitee $softwareVisiteeIn);
}

class BookVisitee extends Visitee
{

    private $author;
    private $title;

    public function __construct($authorIn, $titleIn)
    {
        $this->author = $authorIn;
        $this->title = $titleIn;
    }

    function getAuthor() {return $this->author;}
    function getTitle() {return $this->title;}
    function accept(Visitor $visitorIn) {
        $visitorIn->visitBook($this);
    }
}

class SoftwareVisitee extends Visitee
{
    private $title;
    private $softwareCompany;
    private $softwareCompanyURL;

    function __construct($titleIn, $softwareCompanyIn, $softwareCompanyURLIn)
    {
        $this->title = $titleIn;
        $this->softwareCompany = $softwareCompanyIn;
        $this->softwareCompanyURL = $softwareCompanyURLIn;
    }

    function getSoftwareCompany() {return $this->softwareCompany;}
    function getSoftwareCompanyURL() {return $this->softwareCompanyURL;}
    function getTitle() {return $this->title;}
    function accept(Visitor $visitorIn) {
        $visitorIn->visitSoftware($this);
    }
}

class PlainDescriptionVisitor extends Visitor {
    private $description = NULL;
    function getDescription() {
        return $this->description;
    }
    function setDescription($descriptionIn) {
        $this->description = $descriptionIn;
    }
    function visitBook(BookVisitee $bookVisiteeIn) {
        $this->setDescription($bookVisiteeIn->getTitle().'. written by '.$bookVisiteeIn->getAuthor());
    }
    function visitSoftware(SoftwareVisitee $softwareVisiteeIn) {
        $this->setDescription($softwareVisiteeIn->getTitle().
            '. made by '.$softwareVisiteeIn->getSoftwareCompany().
            '. website at '.$softwareVisiteeIn->getSoftwareCompanyURL());
    }
}

class FancyDescriptionVisitor extends Visitor {
    private $description = NULL;
    function getDescription() { return $this->description; }
    function setDescription($descriptionIn) {
        $this->description = $descriptionIn;
    }
    function visitBook(BookVisitee $bookVisiteeIn) {
        $this->setDescription($bookVisiteeIn->getTitle().
            '...!*@*! written !*! by !@! '.$bookVisiteeIn->getAuthor());
    }
    function visitSoftware(SoftwareVisitee $softwareVisiteeIn) {
        $this->setDescription($softwareVisiteeIn->getTitle().
            '...!!! made !*! by !@@! '.$softwareVisiteeIn->getSoftwareCompany().
            '...www website !**! at http://'.$softwareVisiteeIn->getSoftwareCompanyURL());
    }
}


writeln('BEGIN TESTING VISITOR PATTERN');
writeln('');

$book = new BookVisitee('Design Patterns', 'Gamma, Helm, Johnson, and Vlissides');
$software = new SoftwareVisitee('Zend Studio', 'Zend Technologies', 'www.zend.com');

$plainVisitor = new PlainDescriptionVisitor();

acceptVisitor($book,$plainVisitor);
writeln('plain description of book: '.$plainVisitor->getDescription());
acceptVisitor($software,$plainVisitor);
writeln('plain description of software: '.$plainVisitor->getDescription());
writeln('');

$fancyVisitor = new FancyDescriptionVisitor();

acceptVisitor($book,$fancyVisitor);
writeln('fancy description of book: '.$fancyVisitor->getDescription());
acceptVisitor($software,$fancyVisitor);
writeln('fancy description of software: '.$fancyVisitor->getDescription());

writeln('END TESTING VISITOR PATTERN');
// =========================
// double dispatch any visitor and visitee objects
function acceptVisitor(Visitee $visiteeIn, Visitor $visitorIn) {
    $visiteeIn->accept($visitorIn);
}

function writeln($lineIn) {
    echo $lineIn."<br/>";
}